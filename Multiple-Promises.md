1.  How to consume multiple promises by chaining?

    -   Consuming multiple promises by chaining involves using multiple `.then()` calls to handle the each promise sequentially.

    ````js
    // example of promise chaining using .then and .catch block

    tempPromise
        .then((result) => {
            console.log("First operation successfull with: ", result);

            // returning another async operation to chain it
            return anotherAsyncOperation1();
        })
        .then((result) => {
            // consuming the result return by anotherAsyncOperation1();
            console.log("Second operation successfull with: ", result);

            // returning another async operation to chain it
            return anotherAsyncOperation2();
        })
        .then((result) =>{
            // consuming the result return by anotherAsyncOperation2();

            console.log("Third operation successfull with: ", result);
        })
        .catch((error) => {
            // catching the error by any of the promise chain

            console.error("Error occurred in promise chain:", error);
        });
    ```
    ````

2.  How to consume multiple promises by `Promise.all`?

    -   To consume multiple promises concurrently and handle them together, you can use `Promise.all()`
    -   It takes an array of promises and returns a single promise that resolves when all of the input promises have resolved, or rejects as soon as one of the promises rejects.

    ```js
    // example of consuming multiple promise using Promise.all()

    Promise.all([tempPromise(), anotherAsyncOperation1(), anotherAsyncOperation2();])
        .then((results) => {
            console.log("All promises fulfilled with results:", results);
        })
        .catch((error) => {
            console.error("One of the promises rejected with error:", error);
        });
    ```

3.  How to do error handling when using promises?

    -   Error handling in promises is crucial to ensure that your code behaves as expected
    -   especially in asynchronous operations where errors may occur without being immediately caught
    -   You can handle errors using the `.catch()` method at the end of your promise chain
    -   or by providing individual rejection handlers with .`then()`.
    -   The example can be seen in the above codeblock.

4.  Why is error handling the most important part of using a promise?

    Error handling is particularly important in promises because unhandled rejections can lead to hard-to-debug issues:

    -   such as silent failures : a situation where an error occurs within a promise chain, but it goes unnoticed and unhandled.
    -   uncaught promise rejection errors, which may crash your application or cause unexpected behavior.

5.  How to promisify an asynchronous callbacks based function - eg. setTimeout, fs.readFile?

    -   To promisify an asynchronous callback-based function like `setTimeout` or `fs.readFile`, you can wrap it in a Promise.

    ```js
    // Promisifying setTimeout

    function setTimeoutPromise(delay) {
        return new Promise((resolve) => {
            setTimeout(resolve, delay);
        });
    }

    // Usage
    setTimeoutPromise(1000).then(() => {
        console.log("Timeout completed after 1 second");
    });
    ```

    ```js
    // Promisifying fs.readFile

    const fs = require("fs");

    function readFileAsync(path, encoding) {
        return new Promise((resolve, reject) => {
            fs.readFile(path, encoding, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    //usage
    readFileAsync("./Tech", "utf-8")
        .then((data) => {
            console.log(data);
        })
        .catch((error) => {
            console.error("Error reading file:", error);
        });
    ```
