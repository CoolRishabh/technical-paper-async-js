function setTimeoutPromise(delay) {
    return new Promise((resolve) => {
        setTimeout(resolve, delay);
    });
}

// Usage
setTimeoutPromise(1000).then(() => {
    console.log("Timeout completed after 1 second");
});
