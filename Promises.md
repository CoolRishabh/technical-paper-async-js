1. What is a Promise?
    - A Promise in JavaScript is an object representing the eventual completion or failure of an asynchronous operation, and its resulting value.
    - It allows you to handle asynchronous operations more easily and cleanly than using callbacks.
2. How to create a new promise ?

    - To create a new Promise, you use the Promise constructor.
    - It takes a function as an argument, with two parameters: resolve and reject.
    - Inside this function, you perform the asynchronous operation, and when it's done, you call resolve with the result if it was successful, or reject with an error if it failed.

```js
// example of creating a new promises

const tempPromise = new Promise((resolve, reject) => {
    // perform async operation

    setTimeout(() => {
        const randNumber = Math.random();

        if (randNumber > 0.6) {
            resolve(randNumber); // resolve
        } else {
            reject(new Error("Number is to low")); // reject
        }
    }, 1000);
});
```

3.  What are different states of a Promise -pending, fulfilled, rejected?

    -   Initially, a Promise is in the pending state.
    -   When the asynchronous operation completes successfully, it goes to the fulfilled state with a value.
    -   If there's an error during the operation, it goes to the rejected state with a reason (typically an Error object).

4.  How to consume an existing promise ?

-   To consume an existing Promise, you use the `.then()` method.
-   This method takes one or two arguments:

    -   the first is a function to execute if the Promise is fulfilled,
    -   the second (optional) is a function to execute if the Promise is rejected.

    ```js
    //example of consuming a promises

    tempPromise.then(
        (result) => {
            console.log("Promise fulfilled with result:", result);
        },
        (error) => {
            console.error("Promise rejected with error:", error);
        }
    );
    ```

5. How to chain promises using .then?

    - We can chain promises using multiple `.then()` calls.
    - Each `.then()` returns a new Promise, allowing you to perform sequential asynchronous operations.

6. How to handle errors in a promise chain using `.catch` finally block in a promise chain?

    - The `.catch()` method is used to handle errors that occur in the Promise chain.
    - It's placed at the end of the chain to catch any errors that occur in any preceding `.then()` callbacks.
    - If any error occurs in the chain, it will be caught by the nearest `.catch()`.

    ```js
    // example of promise chaining using .then and .catch block

        tempPromise
        .then((result) => {
            console.log("First operation successfull with: ", result);

            // returning another async operation to chain it
            return anotherAsyncOperation();
        })
        .then((result) => {
            // consuming the result return by anotherAsyncOperation();

            console.log("Second operation successfull with: ", result);
        })
        .catch((error) => {
            console.error("Error occurred in promise chain:", error);
        });
    ```

7. What happens when an Error gets thrown inside `.then` when there is a `.catch`?
    - When an error is thrown inside a `.then()` callback and there's a `.catch()` handler
    - The control flow will jump directly to the nearest `.catch()`.

8. What happens when an Error gets thrown inside `.then` when there is no `.catch`?

    - If there's no `.catch()` handler, the error will propagate to the global context, potentially causing an unhandled promise rejection error.
    - `.finally()` is a method available on Promises that allows you to run some code regardless of whether the Promise was fulfilled or rejected.
    - It's often used for cleanup tasks.
9. Why must `.catch` be placed towards the end of the promise chain?
    - `.catch()` must be placed towards the end of the promise chain to ensure that:
      - any errors occurring in the chain are properly caught and handled
      - preventing unhandled promise rejections
      - allowing for centralized error handling.