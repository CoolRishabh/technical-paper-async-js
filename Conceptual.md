# Conceptual

## 1. How JavaScript Executes Code?

When JavaScript code is encountered by a web browser, it undergoes the following steps for execution:

-   **Parsing**: The browser parses the HTML document, identifying JavaScript code within `<script>` tags or referenced external script files.

-   **Loading**: External scripts are loaded asynchronously, while inline scripts are parsed along with the HTML.

-   **Execution**:

    -   **Synchronous Execution**: Inline scripts are executed synchronously, halting parsing and rendering until completion.
    -   **Asynchronous Execution**: External scripts execute asynchronously, allowing parsing and rendering to continue while the script loads.

-   **Event Queue and Event Loop**: JavaScript is single-threaded, managing asynchronous operations through the event loop. Completed asynchronous tasks push their callback functions to the event queue.

-   **Execution Context**: Each function call creates an execution context, including variables, scope chain, and reference to the outer context.

-   **Call Stack**: JavaScript employs a call stack to manage function calls and execution contexts. Functions are pushed onto the stack when called and popped off when completed.

-   **Event Handling**: When the call stack empties, the browser checks the event queue, executing callbacks in FIFO order.

-   **Rendering**: After executing JavaScript and updating the DOM, the browser re-renders the page if necessary, ensuring a responsive user experience.

## 2. Differences Between Sync & Async?

Web Browser APIs facilitate synchronous and asynchronous programming:

-   **Asynchronous Programming**:

    -   **Non-blocking**: Allows multiple related operations to run concurrently without blocking further execution.
    -   **Use Case**: Ideal for tasks like network communication, where waiting for one operation to complete would slow overall performance.

-   **Synchronous Programming**:
    -   **Blocking**: Executes operations sequentially, with one task completing before the next begins.
    -   **Use Case**: Suitable for reactive systems, ensuring strict sequence adherence and predictable outcomes.

## 3. Ways to Make Code Async?

JavaScript offers multiple approaches for asynchronous programming:

-   **Callbacks**: Functions passed as arguments to handle asynchronous operations' completion.

-   **Promises**: Objects representing the eventual completion or failure of an asynchronous operation, enabling chaining and error handling.

-   **Async/Await**: Syntactic sugar for working with Promises, providing cleaner code with asynchronous functions and the `await` keyword.

-   **Event Listeners**: In the browser environment, defining functions to execute upon specific events like user interactions or resource loading.

## 4. Understanding Web Browser APIs

Web Browser APIs provide tools for web application development:

-   **DOM APIs**: Manipulate HTML and CSS dynamically, creating, modifying, or removing elements.

-   **Canvas and WebGL APIs**: Render 2D and 3D graphics programmatically within HTML canvas elements.

-   **WebRTC API**: Enable real-time communication capabilities like video conferencing and peer-to-peer file sharing.

-   **Geolocation API**: Access user's geographic location for location-aware web applications.

-   **Web Storage APIs**: Store data persistently or temporarily on the client-side browser.

-   **Fetch API**: Modern alternative to XMLHttpRequest for making HTTP requests, leveraging Promises for asynchronous handling.

## 5. Event Loop Mechanism in JavaScript

JavaScript's event loop manages asynchronous tasks:

-   **Call Stack**: Records function calls and execution contexts, managing the program's flow.

-   **Callback Queue**: Holds completed asynchronous tasks' callback functions for execution.

-   **Event Loop**: Continuously checks the call stack and callback queue, ensuring efficient handling of asynchronous operations.

-   **Microtask Queue**: Stores microtasks (e.g., Promises) to be executed after the current script but before the next rendering, ensuring timely execution of critical tasks.
