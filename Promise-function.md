## Promise Methods

### 1. **Promise.resolve**
This method returns a Promise object that resolves with the provided value.

    ```js
        const resolvedPromise = Promise.resolve('Data Resolved');

        resolvedPromise.then((result) => {
        console.log(result); // Output: Data Resolved
        });
    ```

### 2. **Promise.reject**

Returns a Promise object that is rejected with the specified reason.

    ```js
        const rejectedPromise = Promise.reject(new Error('Error Rejected'));

        rejectedPromise.catch((error) => {
        console.error(error.message); // Output: Error Rejected
        });
    ```

### 3. **Promise.all**

This method returns a Promise that resolves when all promises in an iterable have resolved, or rejects with the reason of the first promise that rejects.

    ```js
        const promiseA = Promise.resolve('First Promise');
        const promiseB = 42;
        const promiseC = new Promise((resolve) => setTimeout(resolve, 100, 'Third Promise'));

        Promise.all([promiseA, promiseB, promiseC])
        .then((results) => {
            console.log(results); // Output: ['First Promise', 42, 'Third Promise']
        });
    ```

### 4. **Promise.allSettled**

This method returns a Promise that resolves after all provided promises have settled, providing an array of result objects representing the outcome of each promise.

    ```js
    const promiseList = [
    Promise.resolve('First Promise'),
    Promise.reject(new Error('Rejected Promise')),
    Promise.resolve('Third Promise'),
    ];

    Promise.allSettled(promiseList)
    .then((results) => {
        console.log(results)
    });
    ```

### 5. **Promise.any**

Returns a Promise that resolves as soon as one of the provided promises fulfills, or rejects if all the promises are rejected.

    ```js
        const task1 = new Promise((resolve, reject) => setTimeout(reject, 500, 'First Promise'));
        const task2 = new Promise((resolve, reject) => setTimeout(reject, 200, 'Rejected Promise'));
        const task3 = new Promise((resolve, reject) => setTimeout(reject, 200, 'Rejected Promise2'));

        Promise.any([task1, task2, task3])
        .then((result) => {
        console.log(result);
        })
        .catch((error) => {
        console.error(error); //[AggregateError: All promises were rejected] [...]
        });

    ```

### 6. **Promise.race**

Returns a Promise that resolves or rejects as soon as one of the promises in the iterable resolves or rejects.

    ```js
        const raceTask1 = new Promise((resolve,reject) => setTimeout(resolve, 500, 'First Promise'));
        const raceTask2 = new Promise((resolve,reject) => setTimeout(reject, 200, 'Rejected Promise'));

        Promise.race([raceTask1, raceTask2])
        .then((result) => {
            console.log(result); // Output: 'Rejected Promise'
        })
        .catch((error) => {
            console.error(error); // This will not be called as one of the promises has already resolved
        });
    ```
